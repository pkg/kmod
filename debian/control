Source: kmod
Section: admin
Priority: important
Maintainer: Marco d'Itri <md@linux.it>
Build-Depends: debhelper-compat (= 13),
  autoconf, automake, libtool,
  pkgconf,
  liblzma-dev (>= 5.4.5-0.2),
  libssl-dev,
  libzstd-dev, zstd <!nocheck>,
  gtk-doc-tools, xsltproc,
  scdoc,
Standards-Version: 4.7.0.0
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/md/kmod.git
Vcs-Browser: https://salsa.debian.org/md/kmod

Package: kmod
Architecture: linux-any
Multi-Arch: foreign
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: tools for managing Linux kernel modules
 This package contains a set of programs for loading, inserting, and
 removing kernel modules for Linux.
 It replaces module-init-tools.

Package: libkmod2
Section: libs
Priority: optional
Architecture: linux-any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Conflicts: dracut-install (<= 103-1)
Description: libkmod shared library
 This library provides an API for insertion, removal, configuration and
 listing of kernel modules.

Package: libkmod-dev
Section: libdevel
Priority: optional
Architecture: linux-any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}, libkmod2 (= ${binary:Version})
Breaks: kmod (<< 33+20240816-2)
Replaces: kmod (<< 33+20240816-2)
Description: libkmod development files
 This package contains the files needed for developing applications that
 use libkmod.

Package: kmod-udeb
Package-Type: udeb
Section: debian-installer
Architecture: linux-any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: libkmod shared library
 This is a minimal version of kmod, only for use in the installation system.

Package: libkmod2-udeb
Package-Type: udeb
Section: debian-installer
Architecture: linux-any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: libkmod shared library
 This is a minimal version of libkmod2, only for use in the installation system.
